<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class StockController extends Controller
{
    //
    private $databaseFile = "database.json";

    public function index()
    {
        $data = $this->GetData();
        $responseData["stocks"] = $data;
        return view('welcome', $responseData);
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'productName' => 'required|max:255',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $stock = [
            "productName" => $request->productName,
            "quantity" => $request->quantity,
            "price" => $request->price,
            "createdOn" => date('Y-m-d h:i:s'),
            'TotalValueNumber' => $request->price * $request->quantity
        ];
        $data = $this->GetData();
        array_push($data, $stock);
        $this->Save($data);
        $responseData["stocks"] = $data;
        return view('welcome', $responseData);
    }

    private function GetData()
    {
        if (!$this->isDBCreated()) {
            $fp = fopen($this->databaseFile, 'w');
            fclose($fp);
            return [];
        }
        $jsonString = file_get_contents($this->databaseFile);
        $data = json_decode($jsonString, true);
        if ($data == null) {
            return [];
        }
        return $data;
    }

    private function isDBCreated()
    {
        if (file_exists($this->databaseFile)) {
            return true;
        } else {
            return false;
        }
    }

    private function Save($data)
    {
        $fp = fopen($this->databaseFile, 'w');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }
}
